import itertools
import paramiko
import yaml

def check_server_password_prompt(ip_address, username, password):
    try:
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        
        ssh_client.connect(ip_address, username=username, password=password)
        
        stdin, stdout, stderr = ssh_client.exec_command("echo 'test'")
        output = stdout.read().decode()
        
        if "Authentication failure" in output:
            with open("output.yaml", "a") as file:
                yaml.dump({ip_address: "password!!"}, file)
        
        ssh_client.close()
    except paramiko.AuthenticationException:
        pass
    except paramiko.SSHException as e:
        pass
    except Exception as e:
        pass

username = "root"
passwords = "123"

for ip_parts in itertools.product(range(0, 255), repeat=4):
    ip = ".".join(map(str, ip_parts))
    check_server_password_prompt(ip, username, password)
